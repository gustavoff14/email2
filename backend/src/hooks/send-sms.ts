// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

import twilio from 'twilio';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
      return async (context: HookContext): Promise<HookContext> => {
    const { result } = context;

    //send SMS
    var accountSid = process.env.ACCOUNT_SID; // Your Account SID from www.twilio.com/console
    var authToken = process.env.AUTH_TOKEN;   // Your Auth Token from www.twilio.com/console

    var twilio = require('twilio');
    var client = new twilio(accountSid, authToken);

    var recipient = result['to']
    client.messages.create({
        body: 'MA OE',
        to: recipient,  // Text this number
        from: '+19197523572' // From a valid Twilio number
    })

    //.then((message) => console.log("Message sent: " + message.sid));

    //console.log("Message sent: " + message.sid)

    return context;
  };
};
