# Use the official lightweight Node.js 12 image.
# https://hub.docker.com/_/node
FROM node:12-alpine

# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy local code to the container image.
COPY . ./

# Install front end dependencies.
WORKDIR frontend
RUN yarn install --ignore-engines
RUN yarn add history@4.10.1 react-ga
RUN yarn build

# Install back end dependencies.
WORKDIR ../backend
RUN npm install

# Run the web service on container startup.
CMD [ "npm", "start" ]
